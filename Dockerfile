FROM debian:stable-slim

ENV DEBIAN_FRONTEND=noninteractive

RUN apt update && \
    apt install -y prosody lua-dbi-mysql && \
 rm -rf "/var/lib/apt/lists/*"

ENTRYPOINT ["/usr/bin/prosody-migrator"]
CMD []
