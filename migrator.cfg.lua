input {
    type = "prosody_sql";
    driver = "MySQL";
    database = "ai_ejabberd";
    username = "ejabberd";
    password = "";
    host = "127.0.0.1";
    port = "3310";
}

output {
    type = "prosody_sql";
    driver = "MySQL";
    database = "ai_prosody";
    username = "prosody";
    password = "";
    host = "127.0.0.1";
    port = "3311";
}
