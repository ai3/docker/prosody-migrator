#!/bin/sh
# Run the prosody migrator

# Run as unprivileged user.
container_uid=$(id -u docker-jabber)
container_gid=$(id -g docker-jabber)
opts="$opts --user=$container_uid:$container_gid"
# Add additional groups that the user is a member of.
for gid in $(id -G docker-jabber); do
    if [ $gid -ne $container_gid ]; then
        opts="$opts --group-add=$gid"
    fi
done


# TODO: move to --log-driver=passthrough once it is supported
# by the Podman version in Debian stable, and then add the -d
# option to get rid of the useless 'podman' process.
exec /usr/bin/podman run \
  --cgroups=disabled \
  --replace \
  --sdnotify=conmon \
  --rm --name jabber-migrator \
  --pull=never \
  --log-driver=none \
  --no-healthcheck \
  $opts \
  --network=host \
  --read-only \
  --mount=type=bind,source=/etc/prosody/migrator.cfg.lua,destination=/etc/prosody/migrator.cfg.lua \
  --security-opt=no-new-privileges \
  --cap-drop=all \
  "$@" \
  registry.git.autistici.org/ai3/docker/prosody-migrator:latest
